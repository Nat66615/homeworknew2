#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Есть словарь координат городов

sites = {
    'Moscow': (550, 370),
    'London': (510, 510),
    'Paris': (480, 480),
}
moscow = sites['Moscow']
london = sites['London']
paris = sites['Paris']

# Составим словарь словарей расстояний между ними
# расстояние на координатной сетке - корень из (x1 - x2) ** 2 + (y1 - y2)**2
moscow_london = round(((moscow[0] - london[0]) ** 2 + (moscow[1] - london[1]) ** 2) ** 0.5)
moscow_paris = round(((moscow[0] - paris[0]) ** 2 + (moscow[1] - paris[1]) ** 2) ** 0.5)
london_paris = round(((london[0] - paris[0]) ** 2 + (london[1] - paris[1]) ** 2) ** 0.5)

distances = {
    'Moscow':
        [
            {'Moscow - London': moscow_london},
            {'Moscow - Paris': moscow_paris}
        ],
    'London':
        [
            {'London - Moscow': moscow_london},
            {'London - Paris': london_paris}
        ],
    'Paris':
        [
            {'Paris - Moscow': moscow_london},
            {'Paris - London': london_paris}
        ],
}
print(distances)

# зачёт!
